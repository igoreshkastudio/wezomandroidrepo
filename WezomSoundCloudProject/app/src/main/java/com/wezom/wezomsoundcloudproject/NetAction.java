package com.wezom.wezomsoundcloudproject;

import android.view.View;

/**
 * Created by игорь on 20.02.2017.
 */

public interface NetAction {

    Object getData(Object object);
    void putData(View view, Object object);

}
