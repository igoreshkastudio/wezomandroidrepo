package com.wezom.wezomsoundcloudproject;

/**
 * Created by игорь on 06.02.2017.
 */

public class AlbumObject {

    private String title;
    private String image;

    public AlbumObject(String title, String image){
        this.title = title;
        this.image = image;

    }

    public String getTitle() {
        return title;

    }

    public String getImage(){
        return image;

    }

}
