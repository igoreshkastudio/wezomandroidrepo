package com.wezom.wezomsoundcloudproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by игорь on 03.02.2017.
 */

public class DataAdapter extends BaseAdapter {

    ArrayList<AlbumObject> objects;
    Context mContext;

    public DataAdapter(Context context, ArrayList<AlbumObject> objects){
        this.mContext = context;
        this.objects = objects;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.genre_layout, null);
        TextView text = (TextView) view.findViewById(R.id.genreTitleId);
        text.setText(objects.get(position).getTitle());
        ImageView icon = (ImageView) view.findViewById(R.id.genreIconId);
        String urlImage = objects.get(position).getImage();
        new Main2Activity.DownloadImageTask(icon).execute(urlImage);
        return view;


    }

    @Override
    public int getCount() {
        return objects.size();
    }

    public Object getItem(int position) {
        return null;

    }

    @Override
    public long getItemId(int position) {
        return 0;

    }

}
