package com.wezom.wezomsoundcloudproject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main2Activity extends AppCompatActivity {

    public static AccountManager manager;

    public final static String urlPlayListInfo = "http://api.soundcloud.com/playlists?client_id=6c22694b7d511f976c5cd61d28975d84";
    HttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Тестировал список альбомов жанра
        ArrayList<AlbumObject> genre = new ArrayList<AlbumObject>();
        for (int i=0; i < 10; i++){
            genre.add(new AlbumObject(String.valueOf(i), ""));

        }
        GridView gridView = (GridView)findViewById(R.id.gridGenres);
        DataAdapter booksAdapter = new DataAdapter(this, genre);
        gridView.setAdapter(booksAdapter);

        //Тестировал спиннер
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("ROCK");
        spinnerArray.add("METAL");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.genresSpinner);
        sItems.setAdapter(adapter);


        client = new DefaultHttpClient();
        HttpGet g = new HttpGet("http://api.soundcloud.com/playlists?client_id=6c22694b7d511f976c5cd61d28975d84");
        try {
            ResponseHandler<String> rp = new BasicResponseHandler();
            String r = client.execute(g, rp);
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(r);
            JsonObject rootObject = jsonElement.getAsJsonObject();
            String message = "";//rootObject.get("message").getAsString();
            System.out.println(message);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        /*Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(urlPlayListInfo)
                .addConverterFactory(GsonConverterFactory.create())
                .build();*/



    }

    //Для скачивания картинки хотел использовать но не работает почему то.
    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();

            }
            return mIcon11;

        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }

    }

    //Для сетевых действий хотел использовать
    public static class NetData extends AsyncTask<Object, Object, Object> {
        View view;
        NetAction action;
        Object data;

        public NetData(View view, NetAction action) {
            this.view = view;
            this.action = action;

        }

        protected Object doInBackground(Object... os) {
            return action.getData(os);

        }

        protected void onPostExecute(Object object) {
            action.putData(view, object);
        }

    }

}
