package com.wezom.wezomsoundcloudproject;

/**
 * Created by игорь on 11.02.2017.
 */

public class SongObject {

    private String title;
    private String image;

    public SongObject(String title, String image){
        this.title = title;
        this.image = image;

    }

    public String getTitle() {
        return title;

    }

    public String getImage(){
        return image;

    }

}
